import interfaces.BuildingType;
import org.python.core.PyObject;
import org.python.core.PyString;

public class main {

    public static void main(String[] args) {

        JythonObjectFactory factory = JythonObjectFactory.getInstance();

        // python模块的java对象实例
        BuildingType building = (BuildingType)factory.createObject( BuildingType.class, "Building");

        // 设置调用模块方法
        building.setBuildingName("BUIDING-A");
        building.setBuildingAddress("100 MAIN ST.");
        building.setBuildingId(1);

        System.out.println(building.toStr());
    }

}