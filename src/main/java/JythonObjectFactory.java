import java.util.logging.Level;
import java.util.logging.Logger;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

public class JythonObjectFactory {
    private static JythonObjectFactory instance = null;
    private static PyObject pyObject = null;

    protected JythonObjectFactory() {

    }

    public static JythonObjectFactory getInstance(){
        if(instance == null){
            instance = new JythonObjectFactory();
        }

        return instance;
    }


    public Object createObject(Object interfaceType, String moduleName){
        Object obj = null;
        PythonInterpreter interpreter = new PythonInterpreter();
        // 加载 python 的 Building 模块源码
        interpreter.execfile("/data/apache-storm-1.2.3/examples/jython_demo/src/main/java/Building.py");

        // 返回python 的 Building 模块
        pyObject = interpreter.get(moduleName);

        // 执行 Building 模块的构造方法,创建 Building 对象
        PyObject newObj = pyObject.__call__();

        // 转换为java对象
        obj = newObj.__tojava__( interfaces.BuildingType.class );

        return obj;
    }

}