package interfaces;

// 定义 python module 的接口
public interface BuildingType {
    public String getBuildingName();
    public String getBuildingAddress();
    public String toStr();
    public int getBuildingId();
    public void setBuildingName(String name);
    public void setBuildingAddress(String address);
    public void setBuildingId(int id);
}
