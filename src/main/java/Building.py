# 载入java 定义的模块接口
from interfaces import BuildingType


# 实现 BuildingType 接口, 实现所有接口方法
class Building(BuildingType):
    def __init__(self):
        self.name = None
        self.address = None
        self.id = -1

    def toStr(self):
        return self.name + " " + self.address + " " + str(self.id)

    def getBuildingName(self):
        return self.name

    def setBuildingName(self, name):
        self.name = name

    def getBuildingAddress(self):
        return self.address

    def setBuildingAddress(self, address):
        self.address = address

    def getBuildingId(self):
        return self.id

    def setBuildingId(self, id):
        self.id = id
